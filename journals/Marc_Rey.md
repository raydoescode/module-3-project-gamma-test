June 26, 2023 (Day 1) -
A lot of prep today for the upcoming project, mapped our MVP to have a clear idea of what we want to do.

June 27, 2023 (Day 2) -
Starting Auth and attempting to finish, then continue on from there.

June 28, 2023, (Day 3) -
Finished Auth now need to start forming the pages for front end as well as some more things for back end regarding the personalized information.

June 29, 2023, (Day 4) -
Working on finishing the rest of the backend and then contuing on to Friday!

June 30, 2023, (Day 5) -
Finishing mob coding for backend specifically backend CRUD, small debugging but moving on to frontend afterwards

July 10, 2023, (Day 6) -
Started coding the frontend, added Login, Home, Signup, and App so far. Going to continue the rest of the day added the baseline/outline of the frontend so that we can work on authentication afterwards. Blockers we have are figuring out the authentication for the frontend, the backend auth works and the information is easily processed and displayed..

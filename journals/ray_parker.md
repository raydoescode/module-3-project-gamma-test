## June 26, 2023
First day with the repository and we organized our thoughts on our trello board. Today we focused on API endpoints and watching the JWTdown FastApi video. We decided on going with PostgreSQL for our database and to begin coding authentication tomorrow. Overall we're more than prepared and I feel confident in our ability to work as a team. We plan on taking turns every thirty minutes to code for the back end. Once we get to the front end we will go into pairs and work on feature branches. We agreed on using the Gong method to annouce pushing and pulling, hoping to minimize merge conflicts and stay up-to-date with the code.

## June 27, 2023
We successfully completed our backend without personalized data, following along Curtis' video and taking turns coding. The wireframe, api design and journals have been added to the repository. Our database is set up and we are connected to both fastapi and react. We are on schedule and did a great job communicating throughout our first day coding.

## June 28, 2023
Our next goal is to finish setting up the tables for our database and creating queries and routers for each. We ran into an issue with connecting the account_id to the rehomer_id. We had to individually delete our tables in the database and pull in the new tables to fix this. We are still looking into why it may not be connecting. On a positive note, we were able to add the DuplicateAccountError HTTPException to the create account function resulting in a 400 if that were to occur.

## June 29, 2023
We're almost finished with our backend. We made progress as a team on the CRUD operations for the dog table in the routers and queries. There were a few erors we ran into and were able to fix most. Update dog is still a work in progress.

## June 30, 2023
We are taking the day to catch up on explorations since we are at a good stopping point. Summer break will be from July 3-7.

## July 10, 2023
The front-end has a good template for a home, app, nav, signup, and login. Tomorrow we will start front-end authentication. Currently we're ahead of schedule and brainstorming what we need to accomplish next.